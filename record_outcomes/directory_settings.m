% Update the next three lines for your movie
movieprefix = 'crp_sigma_gadxt';
moviepos = 'xy13';
moviepath = '~/Desktop/gadX_example/';


segpath = [moviepath, 'segmented', moviepos, '/'];

if ~exist(segpath, 'dir')
  mkdir(segpath);
end


file_list=dir([moviepath, movieprefix, '*', moviepos, 'c1.tif']);
file1 = file_list(1).name;

frameN = length(file1) - length(movieprefix) - length(moviepos) - length('c1.tif');
 
