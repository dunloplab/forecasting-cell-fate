import numpy as np
import pandas as pd
from os import listdir
import scipy.io as sio


def main():
    name_list = ['acrAB', 'codB', 'crp', 'dnaQ', 'fis', 'gadX', 'hdeA', 'inaA', 'MG1655', 'nopromoter', 'ompF', 'purA', 'rob', 'rrnBP1', 'sig70', 'sodA', 'soxS'];

    path = 'snaps/data_snaps_10ms_Imane/'

    # initialize with "fake" data point for gadXLB, which doesn't exist in the snapshot data
    df1 = pd.DataFrame([[0,'gadXLB'],[1, 'gadXLB']], columns=['Fluor1 mean','Strain'])

    for u,strain_name in enumerate(name_list):
        folder_name = 'collected_' + strain_name

        file_names = listdir(path + folder_name)
        print('Loading folder: ', folder_name)

        for v,file_name in enumerate(file_names):
            print('Loading: ', path +  folder_name + '/' + file_name)
            mat_contents = sio.loadmat(path + folder_name + '/' + file_name)

            # Adjust to match the names NAR used
            strain = name_list[u]
            if strain == 'inaA':
                strain_convert = 'InaA'
            elif strain == 'soxS':
                strain_convert = 'SoxS'
            elif strain == 'rrnBP1':
                strain_convert = 'rrnbp1'
            elif strain == 'sig70':
                strain_convert = 'sigma70'
            elif strain == 'codB':
                strain_convert = 'CodB'
            elif strain == 'fis':
                strain_convert = 'Fis'
            else:
                strain_convert = name_list[u]

            df2 = pd.DataFrame(mat_contents['data'], columns=[x[0] for x in mat_contents['def'][0]])
            df2 = df2.loc[:,['Strain','Fluor1 mean']];
            df2 = df2.dropna(subset=['Fluor1 mean'])
            df2['Strain'] = strain_convert

            df1 = pd.concat([df1,df2], ignore_index=True)

    df1.to_csv('../dataframes/snapframe.csv')


if __name__ == "__main__":
    main()
