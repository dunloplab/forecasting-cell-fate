import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

import warnings
warnings.filterwarnings('ignore')
import seaborn as sns
sns.set_style('white')

# information and entropy calculators
def information_calculator(px):
    vals = 1 - (-px * np.log2(px) - (1 - px) * np.log2(1 - px))
    vals[np.isnan(vals)] = 1
    return vals


def entropy_calculator(px):
    vals = (-px * np.log2(px) - (1 - px) * np.log2(1 - px))
    vals[np.isnan(vals)] = 0
    return vals

def main():
    data = pd.read_csv('dataframes/carbframe.csv')
    data['Time of Death'][data['Time of Death'] == 0] = np.nan
    data['Time of Death'][data['Time of Death'] == -1] = 64
    data = data[~np.isnan(data['Time of Death'])]
    data['alive'] = np.nan


    plt.close('all')
    fig, ax = plt.subplots()
    sizefig, sizeax = plt.subplots()
    strainz = ['purA',
               'InaA',
               'rob',
               'gadX',
               'hdeA',
               'crp',
               'tonB',
               'SoxS',
               'rrnbp1',
               'acrAB',
               'sigma70',
               'sodA',
               'CodB',
               'ompF',
               'dnaQ',
               'Fis']
    datar = data[data['Strain'].isin(strainz)]
    InitialAu = datar['Area birth']
    Deathtime = datar['Time of Death']
    d = {'InitialAu': InitialAu, 'DeathTime': Deathtime}
    df = pd.DataFrame(data=d)
    df = df.dropna()
    s = {'fluor': df['InitialAu'], 'death': 5 * df['DeathTime']}
    sd = pd.DataFrame(s)
    sd = sd.sort_values(by='fluor')

    # number of fluorophore splits
    n_splits = 10
    rez = np.array_split(sd, n_splits)

    # number of time columns
    column_num = 61
    bins = np.linspace(0, 300, column_num) - 1
    grid = np.zeros((n_splits, column_num - 1))
    labels = np.zeros((n_splits, column_num - 1))
    mins = []

    for x in range(n_splits):
        mino, maxo = min(rez[x].fluor), max(rez[x].fluor)
        labels[x, :] = np.histogram(rez[x].death, bins=bins)[0]
        mins.append(str((x + 1) * 10) + '%')
        grid[x, :] = np.cumsum(np.histogram(rez[x].death, bins=bins)[0] / len(rez[x].death))

    sns.heatmap(np.flipud(grid) * 100, yticklabels=np.flipud(mins), ax=sizeax, xticklabels=[int(u) for u in bins[1:]],
                cmap="rainbow", vmin=0, vmax=100, cbar_kws={'label': '% chance of death'})

    information_time = np.nanmean(information_calculator(grid), 0) - information_calculator(np.mean(grid, 0))
    sizeax.set_title('Cell Size as an indicator')
    sizefig.savefig('figures/heatmaps/size_heatmap.pdf', dpi=300)
    ax.plot(np.arange(len(information_time)) * 5, information_time, color='black')
    ax.set_ylim([0, 0.2])
    fig.savefig('figures/size_traj.pdf')

if __name__ == '__main__':
    main()