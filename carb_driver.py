import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import gaussian_kde
import matplotlib.cm as cm
import matplotlib.patches as mpatches
import warnings
warnings.filterwarnings('ignore')
import seaborn as sns
sns.set_style('white')


# information and entropy calculators
def information_calculator(px):
    vals = 1 - (-px * np.log2(px) - (1 - px) * np.log2(1 - px))
    vals[np.isnan(vals)] = 1
    return vals


def entropy_calculator(px):
    vals = (-px * np.log2(px) - (1 - px) * np.log2(1 - px))
    vals[np.isnan(vals)] = 0
    return vals


def main():
    ### Loading snap data for comparison at the same exposure times
    snapdata = pd.read_csv('dataframes/snapframe.csv')
    ### Loading all carb data for all the track outcomes stuff
    data = pd.read_csv('dataframes/carbframe.csv')
    data['Time of Death'][data['Time of Death'] == 0] = np.nan
    data['Time of Death'][data['Time of Death'] == -1] = 64
    data = data[~np.isnan(data['Time of Death'])]
    data['alive'] = np.nan




    # initialize figures
    barfig, barax = plt.subplots()
    fig, ax = plt.subplots(2, 1, figsize=(4, 8), sharex=True, sharey=True)
    scatterfig, scatterax = plt.subplots(1, 3, figsize=(12, 4), sharey=True)
    megafig, megax = plt.subplots(4, 5, figsize=(5 * 3, 4 * 3), sharex=True, sharey=True)
    megahist, megahistax = plt.subplots(4, 5, figsize=(5 * 3, 4 * 3))
    megahist10ms, megahist10msax = plt.subplots(4, 5, figsize=(5 * 3, 4 * 3))
    lt50s, lt50ax = plt.subplots()
    killingcurve, killingax = plt.subplots()
    gadxkilling, gadxkillingax = plt.subplots()
    gadxcompare, gadxcompareax = plt.subplots()


    strainz = ['purA',
               'InaA',
               'rob',
               'gadX',
               'hdeA',
               'crp',
               'SoxS',
               'rrnbp1',
               'acrAB',
               'sigma70',
               'sodA',
               'CodB',
               'ompF',
               'dnaQ',
               'Fis',
               'nopromoter',
               'gadXLB']

    # get autofluorescence for snap data
    autofluor = snapdata[snapdata['Strain'] == 'MG1655']['Fluor1 mean']
    autofluor = autofluor.dropna()
    autofluor = autofluor.sort_values()
    autofluor = autofluor.iloc[:round(len(autofluor)//10)] # only use the bottom 10% of the values, otherwise means for some strains are negative

    # strainz=ciprodata['Strain'].unique()
    colorz = cm.tab20(np.linspace(0, 1, len(strainz)))
    values = []
    recs = []
    cvs = []
    means = []
    ld50s = []
    killvariance = []
    fronts = []
    n_splits = 10 
    slicecolors = cm.cool(np.linspace(0, 1, n_splits+1))
    for z, strain in enumerate(strainz):
        print("Analyzing strain", strain)

        snapdatum = snapdata[snapdata['Strain'] == strain]['Fluor1 mean']
        snapdatum = snapdatum - np.mean(autofluor)
        means.append(np.mean(snapdatum))
        cvs.append(np.std(snapdatum) / np.mean(snapdatum))
        singlefig, singlheat = plt.subplots()

        density10ms = gaussian_kde(snapdatum)
        xs10ms = np.linspace(min(snapdatum), max(snapdatum), 200)
        megahist10msax[int(np.floor(z / 5)), z % 5].plot(xs10ms, density10ms(xs10ms), color='#d3d3d3')
        megahist10msax[int(np.floor(z / 5)), z % 5].set_title(strain)
        megahist10msax[int(np.floor(z / 5)), z % 5].set_xlim([-2000,40000])
        megahist10msax[int(np.floor(z / 5)), z % 5].set_ylim(bottom=0)
        megahist10msax[int(np.floor(z / 5)), z % 5].ticklabel_format(axis='y', style='sci', scilimits=(-1,1))

        # slice the data
        sliceo = data[data['Strain'] == strain]
        # remove any nans
        sliceo = sliceo[~np.isnan(sliceo['Time of Death'])]

        # calculate the total number of cells
        cell_tot = len(sliceo)

        # compute the cell death over time
        celldeath = []
        killing_matrix = np.zeros((5, 61))

        arrays = np.array_split(sliceo, 5)
        for l, ara in enumerate(arrays):
            for t in range(61):
                cell_tot = len(ara)
                killing_matrix[l, t] = (cell_tot - len(ara[ara['Time of Death'] <= t])) / cell_tot

        meano = np.mean(killing_matrix, 0)
        stdo = np.std(killing_matrix, 0)

        try:
            ind = next(x[0] for x in enumerate(meano) if x[1] < .5)

        except:
            print(strain + ' cells do not die enough')
            ind = 61
        ld50s.append(ind * 5)

        killingax.plot(np.arange(61) * 5, meano, color=colorz[z])

        # density plots

        killingax.fill_between(np.arange(61) * 5, meano - stdo, meano + stdo, color=colorz[z], alpha=0.5)
        if strain == 'gadX':
            gadxkillingax.plot(np.arange(61) * 5, meano, color=colorz[z])
            gadxkillingax.fill_between(np.arange(61) * 5, meano - stdo, meano + stdo, color=colorz[z], alpha=0.5)

        # compute the information over time as a function of initial fluorescence
        InitialAu = sliceo['Fluor1 mean']

        density = gaussian_kde(InitialAu)
        xs = np.linspace(min(InitialAu), max(InitialAu), 200)
        density.covariance_factor = lambda: .25
        density._compute_covariance()
        megahistax[int(np.floor(z / 5)), z % 5].plot(xs, density(xs), color='#d3d3d3')

        # megahistax[int(np.floor(z/5)),z%5].axis('off')
        megahistax[int(np.floor(z / 5)), z % 5].set_title(strain)

        Deathtime = sliceo['Time of Death']
        d = {'InitialAu': InitialAu, 'DeathTime': Deathtime}
        df = pd.DataFrame(data=d)
        df = df.dropna()
        s = {'fluor': df['InitialAu'], 'death': 5 * df['DeathTime']}
        sd = pd.DataFrame(s)
        sd = sd.sort_values(by='fluor')

        # number of fluorophore splits
        rez = np.array_split(sd, n_splits)

        # number of time columns
        column_num = 61
        bins = np.linspace(0, 300, column_num) - 1
        grid = np.zeros((n_splits, column_num - 1))
        labels = np.zeros((n_splits, column_num - 1))
        mins = []

        for r, rezo in enumerate(rez):
            point = rezo['fluor'].iloc[0]
            megahistax[int(np.floor(z / 5)), z % 5].plot(point * np.ones(100), np.linspace(0, max(density(xs)), 100),
                                                     linewidth=1, color=slicecolors[r])
        megahistax[int(np.floor(z / 5)), z % 5].plot(d['InitialAu'].max() * np.ones(100), np.linspace(0, max(density(xs)), 100),
                                                 linewidth=1, color=slicecolors[r+1])
        megahistax[int(np.floor(z / 5)), z % 5].ticklabel_format(axis='y', style='sci', scilimits=(-1,1))

        for x in range(n_splits):
            mino, maxo = min(rez[x].fluor), max(rez[x].fluor)
            labels[x, :] = np.histogram(rez[x].death, bins=bins)[0]
            mins.append(str((x + 1) * 10) + '%')
            grid[x, :] = np.cumsum(np.histogram(rez[x].death, bins=bins)[0] / len(rez[x].death))

        sns.heatmap(np.flipud(grid) * 100, yticklabels=np.flipud(mins), ax=singlheat,
                xticklabels=[int(u) for u in bins[1:]], cmap="rainbow", vmin=0, vmax=100,
                cbar_kws={'label': '% chance of death'})

        sns.heatmap(np.flipud(grid) * 100, cmap="rainbow", ax=megax[int(np.floor(z / 5)), z % 5], cbar=False, vmin=0,
                vmax=100)
        # megax[int(np.floor(z/5)),z%5].axis('off')
        megax[int(np.floor(z / 5)), z % 5].set_title(strain)
        singlefig.savefig('figures/heatmaps/' + strain + '_heatmap.pdf', dpi=300)
        killvariance.append(np.mean(np.std(grid, 0)))

        # compute the information over time from the heatmaps
        information_time = np.nanmean(information_calculator(grid), 0) - information_calculator(np.mean(grid, 0))

        # add rectangle for color match and plot the information over time
        recs.append(mpatches.Rectangle((0, 0), 1, 1, fc=colorz[z]))
        if strain in ['purA', 'InaA', 'rob', 'gadX']:
            ax[0].plot(np.arange(len(information_time)) * 5, information_time, color=colorz[z], label=strain)
        else:
            ax[1].plot(np.arange(len(information_time)) * 5, information_time, color=colorz[z], label=strain)
        # log the maximum information value of the trajectory for comparison among strains
        values.append(np.max(information_time))

        if strain in ['gadX', 'gadXLB']:
            gadxcompareax.plot(np.arange(len(information_time)) * 5, information_time, color=colorz[z], label=strain)

    # plot the bargraph of information rankings
    d = {'values': values, 'strains': strainz, 'means': means, 'cvs': cvs, 'ld50s': ld50s, 'killvariance': killvariance}
    bardf = pd.DataFrame(data=d)
    bardf.sort_values('values', ascending=False).plot.bar(x='strains', y='values', ax=barax, color='#e684ae')
    barax.set_ylabel('Information (bits)')
    barax.set_title('Maximum Mutual Information')

    # scatter plots
    scatterax[0].scatter(bardf['means'], bardf['values'], marker='o', s=200, color=colorz)
    scatterax[0].set_xlabel('Mean Expression (CFP AU)')
    print('mean and values corrcoef: ', np.corrcoef(bardf['means'], bardf['values'])[0, 1])

    scatterax[1].scatter(bardf['cvs'], bardf['values'], marker='o', s=200, color=colorz)
    scatterax[1].set_xlabel('Coefficient of Variation')
    print('cvs and values corrcoef: ', np.corrcoef(bardf['cvs'], bardf['values'])[0, 1])

    scatterax[2].scatter(bardf['ld50s'], bardf['values'], marker='o', s=200, color=colorz)
    scatterax[2].set_xlabel('Time to Reach 50% Cell Death')
    print('ld50s and values corrcoef: ', np.corrcoef(bardf['ld50s'], bardf['values'])[0, 1])

    scatterax[0].set_ylabel('Information (bits)')
    lgd = scatterax[2].legend(recs, strainz, title='Reporter', loc=0, ncol=2, bbox_to_anchor=(2, 1))

    # axis for killing curves
    killingax.set_xlabel('Time (minutes)')
    killingax.set_ylabel('Percentage of Cells Alive')
    killingax.legend(recs, strainz, title='Reporter', loc=1, ncol=2, bbox_to_anchor=(1.5, 1))

    gadxkillingax.set_xlabel('Time (minutes)')
    gadxkillingax.set_ylabel('Percentage of Cells Alive')

    for o in range(2):
        ax[o].set_xlabel('Time (minutes)')
        ax[o].set_ylabel('Information (bits)')
    barfig.savefig('figures/bar.pdf')
    scatterfig.savefig('figures/scatter.pdf',bbox_extra_artists=(lgd,), bbox_inches='tight')
    barfig.savefig('figures/bar.pdf')

    fig.savefig('figures/information_overtime.pdf')
    killingcurve.savefig('figures/kilingcurve.pdf')
    gadxkilling.savefig('figures/gadxkilling.pdf')
    megafig.savefig('figures/megaplot.pdf', dpi=300)

    fig, barax = plt.subplots()

    # axis for gadX comparison
    gadxcompareax.set_xlabel('Time (minutes)')
    gadxcompareax.set_ylabel('Information (bits)')
    gadxcompareax.set_ylim([0,0.2])
    gadxcompareax.legend()
    gadxcompare.savefig('figures/gadxcompare.pdf')

    megahist.savefig('figures/hists.pdf', dpi=300)
    megahist10ms.savefig('figures/hists10ms.pdf', dpi=300)

    plt.close('all')

if __name__ == '__main__':
    main()
