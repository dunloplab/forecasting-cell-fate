import numpy as np
import pandas as pd
from os import listdir
import scipy.io as sio


def main():

    path = 'carb_data/collected_gadX_Nick_growth/'
    names = listdir(path)

    for name in names:
        print(name)
        print('loading ', path, name)
        mat_contents = sio.loadmat(path + name)
        df = pd.DataFrame(mat_contents['data'], columns=[x[0] for x in mat_contents['def'][0]])
        df = df.loc[:,['Strain','Fluor1 mean','Fluor2 mean', 'dL max', 'dL min', 'Growth Rate', 'Time of Death']];

        df['Strain'] = 'gadXgrowth'

        if 'df_all' in locals():
            df_all = pd.concat([df_all, df], ignore_index=True)
        else:
            df_all = df

    df_all = df_all.dropna()

    # do some quality control to get rid of cells that grow or shrink unrealistically
    idx = df_all[(df_all['dL max'] > 7)].index
    df_all.drop(idx, inplace=True)
    idx = df_all[(df_all['dL min'] < -3)].index
    df_all.drop(idx, inplace=True)

    df_all.to_csv('../dataframes/carb_gadXgrowth_frame.csv')

if __name__ == "__main__":
    main()
