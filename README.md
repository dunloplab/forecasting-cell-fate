# Forecasting-2019
Scripts and data necessary to produce all figures from Rossi, El Meouche, Dunlop - Communications Biology 2019



##### Code for generating figures
carb_driver.py
Generates many of the carbenicillin-related figures, including heatmaps, information over time plots, peak information bar charts, fluorescence histograms, and scatter plots.

cipro_driver.py
Same as carb_driver.py, but for ciprofloxacin data.

computational_heatmaps.py
Generates example heatmaps from supplementary.

gadXgrowth.py
Heatmap and information over time for growth rate using GadX data.

permutation_test_carb.py
Randomizes the data and recalculates information. Also contains code to analyze effect of bin size.

size.py
Heatmap and information over time for cell size data.




##### Code for generating data, starting from microscopy images

record_outcomes/
This folder contains Matlab code that allows users to record the time point when a cell dies. This code calls SuperSegger (DOI:10.1111/mmi.13486), which must be present and properly configured for it to run. The two main functions that are used in this work are:

record_outcomes.m
Which gets the fluorescence value from the initial movie frame and then provides a GUI that allows the user to add the time of death into the SuperSegger data files.

record_outcomes_growth.m
Which gets the growth rate (estimated using the first 5 movie frames) and then provides a GUI that allows the user to add the time of death into the SuperSegger data files.




##### Data
dataframes/
Contains the raw data for the paper. These are .csv files that contain information about the strain, fluorescence, time of death, etc. Notes on naming: carb = carbenicillin, cipro = ciprofloxacin, snap = all data imaged at the same exposure time in a snapshot (as opposed to the other data, which are movies).

data/
Code in this folder will regenerate the dataframes .csv files. This folder contains three subfolders (carb_data/, cipro_data/, snaps/) each of which contain subfolders organized by reporter. The files inside these folders are clist outputs from SuperSegger, which were generated using the record_outcomes code described above.

In addition, the folder has several files (*_generator.py) all of which take the data from the clist files and organize it into a dataframe.
