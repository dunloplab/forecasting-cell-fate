import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import gaussian_kde
import matplotlib.cm as cm
import matplotlib.patches as mpatches
import warnings
warnings.filterwarnings('ignore')
import seaborn as sns
sns.set_style('white')
# information and entropy calculators
def information_calculator(px):
    vals = 1 - (-px * np.log2(px) - (1 - px) * np.log2(1 - px))
    vals[np.isnan(vals)] = 1
    return vals


def entropy_calculator(px):
    vals = (-px * np.log2(px) - (1 - px) * np.log2(1 - px))
    vals[np.isnan(vals)] = 0
    return vals

def main():
    ciprodata = pd.read_csv('dataframes/ciproframe.csv')
    strains = ciprodata['Strain'].unique()

    # Cipro analysis
    barfig, barax = plt.subplots()
    fig, ax = plt.subplots(figsize=(6, 10))
    megafig, megax = plt.subplots()
    barfig, barax = plt.subplots()
    strains = ciprodata['Strain'].unique()

    megafig, megax = plt.subplots(4, 5, figsize=(5 * 3, 4 * 3), sharex=True, sharey=True)

    colorz = cm.tab20(np.linspace(0, 1, len(strains)))
    cipvalues = []

    strainz = strains
    recs = []
    for z, strain in enumerate(strainz):
        singlfig, singlax = plt.subplots()
        sliceo = ciprodata[ciprodata['Strain'] == strain]
        sliceo = sliceo[~np.isnan(sliceo['Time of Death'])]
        sliceo['Time of Death'][sliceo['Time of Death'] == -1] = 62
        sliceo['Time of Death'][sliceo['Time of Death'] == 0] = 62
        maxval = len(sliceo)
        traj = []

        for t in range(61):
            traj.append((maxval - len(sliceo[sliceo['Time of Death'] <= t])) / maxval)
        InitialAu = sliceo['Fluor1 mean']
        Deathtime = sliceo['Time of Death']

        d = {'InitialAu': InitialAu, 'DeathTime': Deathtime}
        df = pd.DataFrame(data=d)

        df = df.dropna()
        s = {'fluor': df['InitialAu'], 'death': 5 * df['DeathTime']}
        sd = pd.DataFrame(s)
        sd = sd.sort_values(by='fluor')
        n_splits = 10
        rez = np.array_split(sd, n_splits)
        column_num = 61
        bins = np.linspace(0, 300, column_num) - 1

        grid = np.zeros((n_splits, column_num - 1))
        labels = np.zeros((n_splits, column_num - 1))
        mins = []

        for x in range(n_splits):
            mino, maxo = min(rez[x].fluor), max(rez[x].fluor)
            grid[x, :] = np.cumsum(np.histogram(rez[x].death, bins=bins)[0] / len(rez[x].death))
            labels[x, :] = np.histogram(rez[x].death, bins=bins)[0]
            mins.append(str((x + 1) * 10) + '%')

        sns.heatmap(np.flipud(grid) * 100, ax=singlax, yticklabels=np.flipud(mins),
                    xticklabels=[int(u) for u in bins[1:]], cmap="rainbow", vmin=0, vmax=100,
                    cbar_kws={'label': '% chance of death'})
        singlax.set_title(strain)
        singlax.set_yticks(np.arange(len(np.flipud(mins))) + 1)
        singlfig.savefig('figures/cipro_figs/' + strain + '_ciproheatmap.pdf', dpi=300)
        sns.heatmap(np.flipud(grid) * 100, cmap="RdYlBu_r", ax=megax[int(np.floor(z / 5)), z % 5], cbar=False, vmin=0,
                    vmax=100)
        megax[int(np.floor(z / 5)), z % 5].axis('off')
        megax[int(np.floor(z / 5)), z % 5].set_title(strain)

        recs.append(mpatches.Rectangle((0, 0), 1, 1, fc=colorz[z]))
        information_time = np.nanmean(information_calculator(grid), 0) - information_calculator(np.mean(grid, 0))

        ax.plot(np.arange(len(information_time)) * 5, information_time, color=colorz[z], label=strain)

        # ax.fill_between(np.arange(column_num-2)*5,  vanilla_traj-error_down[:-1], vanilla_traj-error_up[:-1], color=colorz[z], alpha=0.5)

        cipvalues.append(np.max(information_time))

    d = {'cipvalues': cipvalues, 'strains': strainz}
    bardf = pd.DataFrame(data=d)
    bardf.sort_values('cipvalues', ascending=False).plot.bar(x='strains',ax=barax)
    barax.set_ylabel('Information (bits)')

    # ax.legend()
    # ax.set_ylim([0,0.12])
    ax.set_xlabel('Time (minutes)')
    ax.set_ylabel('Information (bits)')
    # barfig.savefig('figures/cipro_figs/bar.pdf')
    ax.legend(recs, strainz, title='Reporter', loc=1, ncol=2, bbox_to_anchor=(1, 1))

    fig.savefig('figures/cipro_figs/information_overtime.pdf')
    megafig.savefig('figures/cipro_figs/megafig.pdf')
    barfig.savefig('figures/cipro_figs/bar.pdf')


if __name__ == '__main__':
    main()