import numpy as np
import matplotlib.pyplot as plt
import warnings
warnings.filterwarnings('ignore')
import seaborn as sns

def main():
    plt.subplots()
    fig, ax = plt.subplots(2, 3, figsize=(12, 8))
    tracefig, traceax = plt.subplots()

    def information_calculator(px):
        vals = 1 - (-px * np.log2(px) - (1 - px) * np.log2(1 - px))
        vals[np.isnan(vals)] = 1
        return vals

    def entropy_calculator(px):
        vals = (-px * np.log2(px) - (1 - px) * np.log2(1 - px))
        vals[np.isnan(vals)] = 0
        return vals

    # number of fluorophore splits
    n_splits = 10
    column_num = 10

    grid = np.zeros((n_splits, column_num - 1))

    vals = np.linspace(0, 1.0, column_num - 1)
    vals = np.append(vals, vals[::-1])

    grid = np.zeros((n_splits, len(vals)))

    # grid[5:,5:]=.2*np.multiply(np.arange(13),np.ones(np.shape(grid[5:,5:])))
    zize = 100
    grid = 1 - np.hstack((np.tril(np.ones(zize)), np.zeros((zize, 1))))

    sns.heatmap(np.flipud(grid) * 100, ax=ax[0, 0], cmap="rainbow", vmin=0, vmax=100, cbar=False)
    ax[0, 0].axis('off')
    #
    information_time = np.nanmean(information_calculator(grid), 0) - information_calculator(np.mean(grid, 0))
    traceax.plot(information_time, label='old')

    grid = np.ones((zize))

    # grid=1-np.tril(np.ones(zize))*np.linspace(0,1,zize)[::-1]

    grid = np.ones((zize, zize))
    for l in range(100):
        for k in range(100):
            grid[l, k] = .5 * (l) / 100 + .5 * (k) / 100
    grid = 1 - grid
    grid = np.fliplr(grid)

    sns.heatmap(np.flipud(grid) * 100, ax=ax[0, 1], cmap="rainbow", vmin=0, vmax=100, cbar=False)
    ax[0, 1].axis('off')
    traceax.set_ylim([0, 1.1])

    information_time = np.nanmean(information_calculator(grid), 0) - information_calculator(np.mean(grid, 0))
    traceax.plot(information_time)

    for x in range(100):
        grid[:, x] = 1 - np.power(1 - (x / 100) * .644, np.arange(100) / 20)
    grid = np.flipud(grid)

    sns.heatmap(np.flipud(grid) * 100, ax=ax[0, 2], cmap="rainbow", vmin=0, vmax=100, cbar=False)
    information_time = np.nanmean(information_calculator(grid), 0) - information_calculator(np.mean(grid, 0))
    ax[0, 2].axis('off')
    traceax.plot(information_time)

    grid = np.ones((zize, zize))
    for x in range(100):
        grid[x, :] = np.linspace(0, 1, 100)
    sns.heatmap(np.flipud(grid) * 100, ax=ax[1, 0], cmap="rainbow", vmin=0, vmax=100, cbar=False)
    ax[1, 0].axis('off')
    information_time = np.nanmean(information_calculator(grid), 0) - information_calculator(np.mean(grid, 0))
    traceax.plot(information_time)

    # grid=1-np.tril(np.ones(zize))*np.linspace(0,1,zize)[::-1]
    grid = 1 - np.tril(np.ones(zize)) * np.linspace(0, 1, zize)[::-1]

    sns.heatmap(np.flipud(grid) * 100, ax=ax[1, 1], cmap="rainbow", vmin=0, vmax=100, cbar=False)
    information_time = np.nanmean(information_calculator(grid), 0) - information_calculator(np.mean(grid, 0))
    traceax.plot(information_time)

    grid = np.ones((zize, zize))

    grid[50:, :] = 0
    sns.heatmap(np.flipud(grid) * 100, ax=ax[1, 2], cmap="rainbow", vmin=0, vmax=100, cbar=False)
    information_time = np.nanmean(information_calculator(grid), 0) - information_calculator(np.mean(grid, 0))
    traceax.plot(information_time)
    fig.savefig('figures/computational_heat.pdf')
    traceax.set_ylim([-0.1, 1.1])
    tracefig.savefig('figures/computational_trace.pdf')

if __name__ == '__main__':
    main()