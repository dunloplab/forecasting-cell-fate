import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import gaussian_kde
import matplotlib.cm as cm
import matplotlib.patches as mpatches
import warnings
warnings.filterwarnings('ignore')
import seaborn as sns
sns.set_style('white')


# information and entropy calculators
def information_calculator(px):
    vals = 1 - (-px * np.log2(px) - (1 - px) * np.log2(1 - px))
    vals[np.isnan(vals)] = 1
    return vals


def entropy_calculator(px):
    vals = (-px * np.log2(px) - (1 - px) * np.log2(1 - px))
    vals[np.isnan(vals)] = 0
    return vals


def main():
    ### Loading all carb data for all the track outcomes stuff
    data = pd.read_csv('dataframes/carb_gadXgrowth_frame.csv')
    data['Time of Death'][data['Time of Death'] == 0] = np.nan
    data['Time of Death'][data['Time of Death'] == -1] = 64
    data = data[~np.isnan(data['Time of Death'])]
    data['alive'] = np.nan

    # initialize figures
    barfig, barax = plt.subplots()
    fig, ax = plt.subplots(2, 1, figsize=(4, 8), sharex=True, sharey=True)
    killingcurve, killingax = plt.subplots()

    colorz = cm.tab20(np.linspace(0, 1, 1))
    values = []
    n_splits = 10 
    slicecolors = cm.rainbow(np.linspace(0, 1, n_splits))

    strain = 'gadXgrowth'

    singlefig, singlheat = plt.subplots()

    # slice the data
    sliceo = data[data['Strain'] == strain]
    # remove any nans
    sliceo = sliceo[~np.isnan(sliceo['Time of Death'])]

    # calculate the total number of cells
    cell_tot = len(sliceo)

    # compute the cell death over time
    celldeath = []
    killing_matrix = np.zeros((5, 61))

    arrays = np.array_split(sliceo, 5)
    for l, ara in enumerate(arrays):
        for t in range(61):
            cell_tot = len(ara)
            killing_matrix[l, t] = (cell_tot - len(ara[ara['Time of Death'] <= t])) / cell_tot

    meano = np.mean(killing_matrix, 0)
    stdo = np.std(killing_matrix, 0)

    try:
        ind = next(x[0] for x in enumerate(meano) if x[1] < .5)

    except:
        print(strain + ' cells do not die enough')
        ind = 61

    killingax.plot(np.arange(61) * 5, meano, color=colorz[0])

    # compute the information over time as a function of initial fluorescence
#If you want to see the heat map for Fluor 1 (cfp) instead of for the growth rate just use the next line and comment the one after it
    #InitialAu = sliceo['Fluor1 mean']
    InitialAu = sliceo['Growth Rate']

    Deathtime = sliceo['Time of Death']
    d = {'InitialAu': InitialAu, 'DeathTime': Deathtime}
    df = pd.DataFrame(data=d)
    df = df.dropna()
    s = {'fluor': df['InitialAu'], 'death': 5 * df['DeathTime']}
    sd = pd.DataFrame(s)
    sd = sd.sort_values(by='fluor')

    # number of fluorophore splits
    rez = np.array_split(sd, n_splits)

    # number of time columns
    column_num = 61
    bins = np.linspace(0, 300, column_num) - 1
    grid = np.zeros((n_splits, column_num - 1))
    labels = np.zeros((n_splits, column_num - 1))
    mins = []

    for x in range(n_splits):
        mino, maxo = min(rez[x].fluor), max(rez[x].fluor)
        labels[x, :] = np.histogram(rez[x].death, bins=bins)[0]
        mins.append(str((x + 1) * 10) + '%')
        grid[x, :] = np.cumsum(np.histogram(rez[x].death, bins=bins)[0] / len(rez[x].death))

    sns.heatmap(np.flipud(grid) * 100, yticklabels=np.flipud(mins), ax=singlheat,
            xticklabels=[int(u) for u in bins[1:]], cmap="rainbow", vmin=0, vmax=100,
            cbar_kws={'label': '% chance of death'})

    singlefig.savefig('figures/growth/' + strain + '_heatmap.pdf', dpi=300)

    # compute the information over time from the heatmaps
    information_time = np.nanmean(information_calculator(grid), 0) - information_calculator(np.mean(grid, 0))

    # add rectangle for color match and plot the information over time
    ax[0].plot(np.arange(len(information_time)) * 5, information_time, color=colorz[0], label=strain)
    # log the maximum information value of the trajectory for comparison among strains
    values.append(np.max(information_time))


    # axis for killing curves
    killingax.set_xlabel('Time (minutes)')
    killingax.set_ylabel('Percentage of Cells Alive')

    for o in range(1):
        ax[o].set_xlabel('Time (minutes)')
        ax[o].set_ylabel('Information (bits)')
        ax[o].set_ylim([0,0.2])
    barfig.savefig('figures/growth/bar.pdf')

    fig.savefig('figures/growth/information_overtime.pdf')
    killingcurve.savefig('figures/growth/kilingcurve.pdf')

    fig, barax = plt.subplots()

    plt.close('all')

if __name__ == '__main__':
    main()
