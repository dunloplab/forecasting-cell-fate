import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import gaussian_kde
import matplotlib.cm as cm
import matplotlib.patches as mpatches
import warnings
warnings.filterwarnings('ignore')
import seaborn as sns
sns.set_style('white')


# information and entropy calculators
def information_calculator(px):
    vals = 1 - (-px * np.log2(px) - (1 - px) * np.log2(1 - px))
    vals[np.isnan(vals)] = 1
    return vals


def entropy_calculator(px):
    vals = (-px * np.log2(px) - (1 - px) * np.log2(1 - px))
    vals[np.isnan(vals)] = 0
    return vals


def main():
    ### Loading all carb data for all the track outcomes stuff
    data = pd.read_csv('dataframes/carbframe.csv')
    data['Time of Death'][data['Time of Death'] == 0] = np.nan
    data['Time of Death'][data['Time of Death'] == -1] = 64
    data = data[~np.isnan(data['Time of Death'])]
    data['alive'] = np.nan

    # initialize figures
    barfig, barax = plt.subplots()
    barbinfig, barbinax = plt.subplots()
    fig, ax = plt.subplots(2, 1, figsize=(4, 8), sharex=True, sharey=True)
    megafig, megax = plt.subplots(4, 5, figsize=(5 * 3, 4 * 3), sharex=True, sharey=True)
    megahist, megahistax = plt.subplots(4, 5, figsize=(5 * 3, 4 * 3))


    strainz = ['purA',
               'InaA',
               'rob',
               'gadX',
               'hdeA',
               'crp',
               'SoxS',
               'rrnbp1',
               'acrAB',
               'sigma70',
               'sodA',
               'CodB',
               'ompF',
               'dnaQ',
               'Fis',
               'gadXLB',
               'nopromoter']
    colorz = cm.tab20(np.linspace(0, 1, len(strainz)))
    maxinfo_mean = []
    maxinfo_cv = []
    maxinfo_sort = []
    frames = []

    for z, strain in enumerate(strainz):
       singlefig, singlheat = plt.subplots()

       # slice the data
       sliceo = data[data['Strain'] == strain]
       # remove any nans
       sliceo = sliceo[~np.isnan(sliceo['Time of Death'])]

       # calculate the total number of cells
       cell_tot = len(sliceo)

       # compute the cell death over time
       celldeath = []
       killing_matrix = np.zeros((5, 61))
       arrays = np.array_split(sliceo, 5)
       for l, ara in enumerate(arrays):
           for t in range(61):
               cell_tot = len(ara)
               killing_matrix[l, t] = (cell_tot - len(ara[ara['Time of Death'] <= t])) / cell_tot

       meano = np.mean(killing_matrix, 0)
       stdo = np.std(killing_matrix, 0)

       try:
           ind = next(x[0] for x in enumerate(meano) if x[1] < .5)
       except:
           print(strain + ' cells do not die enough')
           ind = 61

       # get data necessary to compute the information over time as a function of initial fluorescence
       InitialAu = sliceo['Fluor1 mean']
       Deathtime = sliceo['Time of Death']
       d = {'InitialAu': InitialAu, 'DeathTime': Deathtime}
       df = pd.DataFrame(data=d)
       df = df.dropna()
       s = {'fluor': df['InitialAu'], 'death': 5 * df['DeathTime']}
       sd = pd.DataFrame(s)

       # number of fluorophore splits
       n_splits = 10

       # number of time columns
       column_num = 61
       bins = np.linspace(0, 300, column_num) - 1
       grid = np.zeros((n_splits, column_num - 1))

       # iterate multiple times to calculate average max informations for randomized orders
       Niterations = 100
       maxinfo = []
       for ii in range(Niterations+1):
           if ii%10 == 0:
               print(strain, ' iteration ', ii)

           mins = []

           if ii != Niterations:
               # Randomize the order for the permutation test
               sdrand = sd.iloc[np.random.permutation(len(sd))]
           else:
               sdrand = sd.sort_values(by='fluor')

           rez = np.array_split(sdrand, n_splits)

           for x in range(n_splits):
               mino, maxo = min(rez[x].fluor), max(rez[x].fluor)
               mins.append(str((x + 1) * 10) + '%')
               grid[x, :] = np.cumsum(np.histogram(rez[x].death, bins=bins)[0] / len(rez[x].death))

           # compute the information over time from the heatmaps
           information_time = np.nanmean(information_calculator(grid), 0) - information_calculator(np.mean(grid, 0))
           if ii != Niterations:
               # log the maximum information value of the trajectory for comparison among strains
               maxinfo.append(np.max(information_time))
           else:
               maxinfo_sort.append(np.max(information_time))

           # Plot heatmaps and information over time for the first iteration
           if ii == 0:
               sns.heatmap(np.flipud(grid) * 100, yticklabels=np.flipud(mins), ax=singlheat,
                   xticklabels=[int(u) for u in bins[1:]], cmap="rainbow", vmin=0, vmax=100,
                   cbar_kws={'label': '% chance of death'})

               sns.heatmap(np.flipud(grid) * 100, cmap="rainbow", ax=megax[int(np.floor(z / 5)), z % 5], cbar=False, vmin=0,
               vmax=100)
               megax[int(np.floor(z / 5)), z % 5].set_title(strain)

               # add rectangle for color match and plot the information over time
               if strain in ['purA', 'InaA', 'rob', 'gadX']:
                   ax[0].plot(np.arange(len(information_time)) * 5, information_time, color=colorz[z], label=strain)
               else:
                   ax[1].plot(np.arange(len(information_time)) * 5, information_time, color=colorz[z], label=strain)
               ax[0].set_ylim([0,0.21])
               ax[1].set_ylim([0,0.21])

       maxinfo_mean.append(np.mean(maxinfo))
       maxinfo_cv.append(np.std(maxinfo))

       # Try different bin sizes 
       # number of fluorophore splits
       n_splits = [5, 10, 15]
       binnum = []
       maxinfo_bin = []
       for Nbin in n_splits:
           binnum.append(Nbin)

           sdbin = sd.sort_values(by='fluor')
           rez = np.array_split(sdbin, Nbin)

           grid = np.zeros((Nbin, column_num - 1))

           for x in range(Nbin):
               mino, maxo = min(rez[x].fluor), max(rez[x].fluor)
               mins.append(str((x + 1) * 10) + '%')
               grid[x, :] = np.cumsum(np.histogram(rez[x].death, bins=bins)[0] / len(rez[x].death))

           # compute the information over time from the heatmaps
           information_time = np.nanmean(information_calculator(grid), 0) - information_calculator(np.mean(grid, 0))
           maxinfo_bin.append(np.max(information_time))
       dbin = {'binnum': binnum, 'maxinfo_bin': maxinfo_bin}
       df = pd.DataFrame(dbin)
       frames.append(df)
       

    # plot the bargraph of information rankings
    d = {'maxinfo_mean': maxinfo_mean, 'strains': strainz, 'maxinfo_sort': maxinfo_sort}
    bardf = pd.DataFrame(data=d)
    bardf.sort_values('maxinfo_sort', ascending=False).plot.bar(x='strains', y=['maxinfo_sort','maxinfo_mean'], yerr=[np.zeros(np.size(maxinfo_cv)), maxinfo_cv], 
        ax=barax, color=['#e684ae','#AAAAAA'], ecolor='gray', capsize=2)
    barax.set_ylim([0, 0.21])
    barax.set_ylabel('Information (bits)')
    barax.set_title('Maximum Mutual Information')

    # ax.legend()
    for o in range(2):
        ax[o].set_xlabel('Time (minutes)')
        ax[o].set_ylabel('Information (bits)')
    barfig.savefig('figures/permutation/bar.pdf')

    fig.savefig('figures/permutation/information_overtime.pdf')
    megafig.savefig('figures/permutation/megaplot.pdf', dpi=300)

    # plot comparision of bin sizes
    bindata = pd.concat(frames, keys=strainz)
    print(bindata)

    color = ["#00bfff"] + ["#0099cc"] + ["#007399"]
    bindata.plot.bar(y='maxinfo_bin', ax=barbinax, color=color)
    #barbinax.set_ylim([0, 0.21])
    barbinfig.savefig('figures/permutation/barbin.pdf')

    plt.close('all')
    fig, barax = plt.subplots()

if __name__ == '__main__':
    main()
